//
//  Echo.h
//  ObjcPluginTest1
//
//  Created by lendle on 2014/3/13.
//  Copyright (c) 2014年 lendle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CDV.h"
@interface Echo : CDVPlugin
- (void)echo:(CDVInvokedUrlCommand*)command;
@end
