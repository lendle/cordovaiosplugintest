//
//  main.m
//  CordovaiOSPluginTest
//
//  Created by lendle on 2014/3/13.
//  Copyright (c) 2014年 lendle. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
