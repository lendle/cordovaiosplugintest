//
//  AppDelegate.h
//  CordovaiOSPluginTest
//
//  Created by lendle on 2014/3/13.
//  Copyright (c) 2014年 lendle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
